#include <iostream>
#include <string>
#include <unistd.h>
int main(int argc, char* argv[]) {
  auto s = R"(Makefile  comp3511_pa1_grading_plan.pdf  deps  json_helper.h  judge  judge.cc  logging.h  ls_fake  ls_fake.cc  myshell  property_tree.h  test1.json  test2.json  test3.json  test4.json)";
  auto s2 = R"(total 3.6M
-rw-r--r-- 1 cjr users  770 Oct 25 02:44 Makefile
-rw-r--r-- 1 cjr users 254K Oct 22 17:59 comp3511_pa1_grading_plan.pdf
drwxr-xr-x 1 cjr users   14 Oct 25 00:33 deps
-rw-r--r-- 1 cjr users 1.5K Oct 25 02:02 json_helper.h
-rwxr-xr-x 1 cjr users 3.1M Oct 25 02:40 judge
-rw-r--r-- 1 cjr users 7.0K Oct 25 02:40 judge.cc
-rw-r--r-- 1 cjr users  14K Oct 24 16:52 logging.h
-rwxr-xr-x 1 cjr users 104K Oct 25 02:46 ls_fake
-rw-r--r-- 1 cjr users 1.2K Oct 25 02:46 ls_fake.cc
-rwxr-xr-x 1 cjr users  22K Oct 24 17:08 myshell
-rw-r--r-- 1 cjr users  517 Oct 25 00:40 property_tree.h
-rw-r--r-- 1 cjr users  177 Oct 24 22:59 test1.json
-rw-r--r-- 1 cjr users  412 Oct 25 02:41 test2.json
-rw-r--r-- 1 cjr users  501 Oct 24 23:02 test3.json
-rw-r--r-- 1 cjr users  488 Oct 24 23:05 test4.json)";
  auto s3 = R"(Makefile
comp3511_pa1_grading_plan.pdf
deps
json_helper.h
judge
judge.cc
logging.h
ls_fake
ls_fake.cc
myshell
property_tree.h
ps_fake
ps_fake.cc
test1.json
test2.json
test3.json
test4.json)";
  if (argc == 1) {
    if (isatty(1)) {
      // many_per_line
      std::cout << s << "\n";
    } else {
      // one_per_line
			std::cout << s3 << "\n";
    }
  } else {
    std::cout << s2 << "\n";
  }
  return 0;
}
