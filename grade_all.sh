#!/bin/bash

for stu in `/bin/ls ../pa01_submission`; do
	#echo $stu 1>&2
	DIR="../pa01_submission/$stu"
	points=`cat $DIR/score.txt | grep 'Total' | awk '{print $4}' | cut -d "/" -f 1 | runhaskell ~/misc/sum_integers.hs`
	echo $stu $points
done
