.PHONY: clean all install uninstall

DEPS_PATH ?= $(shell pwd)/deps
CFLAGS += -std=c++17 -g -Wall -Werror
INCPATH += -I$(DEPS_PATH)/include

DEBUG := 1

ifeq ($(DEBUG), 1)
	CFLAGS += -DJUDGE_DEBUG -DPRISM_LOG_STACK_TRACE -DPRISM_LOG_STACK_TRACE_SIZE=128
else
	CFLAGS += -O2
endif


URL := https://raw.githubusercontent.com/crazyboycjr/deps/master/build
WGET ?= wget

BOOST_PT_TREE := $(DEPS_PATH)/include/boost/property_tree/json_parser.hpp

APP := judge ls_fake ps_fake c_comment

all: $(APP)

judge: judge.cc json_helper.h property_tree.h logging.h $(BOOST_PT_TREE)
	$(CXX) $(INCPATH) $(CFLAGS) $< -o $@ -static -static-libgcc -static-libstdc++

%_fake: %_fake.cc
	$(CXX) $(INCPATH) $(CFLAGS) $< -o $@

c_comment: c_comment.cc
	$(CXX) $(INCPATH) $(CFLAGS) $< -o $@

clean:
	rm -rfv $(APP)

$(BOOST_PT_TREE):
	$(eval FILE=boost-property_tree-1.70.0.tar.gz)
	$(eval DIR=property_tree)
	rm -rf $(FILE) $(DIR)
	$(WGET) $(URL)/$(FILE) && tar --no-same-owner -zxf $(FILE) && mkdir -p $(DEPS_PATH) && cp -r $(DIR)/* $(DEPS_PATH)/
	rm -rf $(FILE) $(DIR)

install: ls_fake ps_fake
	cp ls_fake /usr/local/bin/ls
	cp ps_fake /usr/local/bin/ps
	#unalias ls

uninstall:
	rm -fv /usr/local/bin/ls
	rm -fv /usr/local/bin/ps
	#alias ls='ls --color=auto'
