#!/bin/bash

tests=(test1.json test2.json test3.json test4.json)

for stu in `/bin/ls ../pa01_submission`; do
	echo $stu 1>&2
	DIR="../pa01_submission/$stu"
	gcc $DIR/myshell.c -o myshell -g -w #-Wall -Wextra
	if [ $? -eq 0 ]; then
		rm -fv $DIR/score.txt
		./c_comment $DIR/myshell.c | tee -a $DIR/score.txt
		for t in ${tests[@]}; do
			echo "Case: $t"
			./judge ./myshell $t | tee -a $DIR/score.txt
		done
	fi
done
