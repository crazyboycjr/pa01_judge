#!/bin/bash

if [ $# -ne 1 ]; then
	echo "Usage: $0 <itsc_name>"
	exit 1
fi

stu=$1
tests=(test1.json test2.json test3.json test4.json)

echo "student: $stu"
DIR="../pa01_submission/$stu"
gcc $DIR/myshell.c -o myshell -g -Wall -Wextra


if [ $? -eq 0 ]; then
	if [[ ! -z $JUDGE_BYHAND ]]; then
		./judge ./myshell
	else
		rm -fv $DIR/score.txt
		./c_comment $DIR/myshell.c | tee -a $DIR/score.txt
		for t in ${tests[@]}; do
			echo "Case: $t"
			./judge ./myshell $t | tee -a $DIR/score.txt
		done
	fi
	echo "The result is in $DIR/score.txt"
fi

