#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <memory.h>

#include "./utils.h"
#include <iostream>
#include <unordered_map>

/*
%option 8bit 
 
%x XS_MCOMM  XS_SCOMM  XS_QUOTE
NL            \r\n|\r|\n
LCTN        (\\{NL})*
 
%%
 
\/{LCTN}\*            ///@ XA_MCOMM_BEGIN
\/{LCTN}\/            ///@ XA_SCOMM_BEGIN
\"|\'                ///@ XA_QUOTE_BEGIN
 
<XS_MCOMM>{
    \*{LCTN}\/        ///@ XA_MCOMM_END
}
 
<XS_SCOMM>{
    {NL}            ///@ XA_SCOMM_END
}
 
<XS_QUOTE>{
    \"|\'            ///@ XA_QUOTE_END
    {NL}            ///@ XA_QUOTE_NL
    \\(.|{NL})
}
 
<*>{
    \\{NL}
}
*/
/* >>>>>>>>>>>>>>>>>>>>>>> Auto generate code begin >>>>>>>>>>>>>>>>>>>>>>>*/
enum 
{
    INITIAL              =    1,
    XS_MCOMM             =    3,
    XS_SCOMM             =    5,
    XS_QUOTE             =    7,
 
    XA_MCOMM_BEGIN       =    1,
    XA_SCOMM_BEGIN       =    2,
    XA_QUOTE_BEGIN       =    3,
    XA_MCOMM_END         =    4,
    XA_SCOMM_END         =    5,
    XA_QUOTE_END         =    6,
    XA_QUOTE_NL          =    7,
 
 
    YY_LASTDFA             =    36,
    YY_JAMBASE           =    45,
};
 
 
static const unsigned char yy_accept[38] = { 0 ,
       0,   0,   0,   0,   0,   0,   0,   0,  11,  10,
       3,  10,  10,  10,   5,   5,   7,   7,   6,  10,
       1,   2,   0,   9,   9,   4,   0,   5,   7,   8,
       8,   8,   0,   0,   0,   0,   0 
};
 
static const unsigned char yy_ec[256] = { 1 ,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   2,
       1,   1,   3,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   4,   1,   1,   1,   1,   5,   1,
       1,   6,   1,   1,   1,   1,   7,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   8,   1,   1,   1,   1,   1,   1,   1,   1,
 
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
 
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
       1,   1,   1,   1,   1 
};
 
static const unsigned char yy_meta[9] = { 0 ,
       1,   1,   1,   1,   1,   1,   1,   1 
};
 
static const unsigned char yy_base[39] = { 0 ,
       0,   0,   5,   0,  11,   0,  17,   0,  44,  45,
      45,  20,  27,  24,  45,  41,  45,  40,  45,  31,
      45,  45,  33,  45,  39,  45,  35,  45,  45,  45,
      45,  38,   0,  37,   0,  14,  45,  14 
};
 
static const unsigned char yy_def[39] = { 0 ,
      37,   1,   1,   3,   3,   5,  37,   7,  37,  37,
      37,  37,  37,  37,  37,  37,  37,  37,  37,  38,
      37,  37,  37,  37,  37,  37,  37,  37,  37,  37,
      37,  37,  12,  12,  14,  14,   0,  37 
};
 
static const unsigned char yy_nxt[54] = { 0 ,
      10,  10,  10,  11,  11,  10,  12,  13,  10,  10,
      14,  10,  15,  16,  30,  35,  10,  10,  17,  18,
      19,  19,  10,  10,  20,  21,  22,  23,  24,  25,
      26,  27,  31,  32,  33,  34,  35,  36,  33,  31,
      24,  29,  28,  37,   9,  37,  37,  37,  37,  37,
      37,  37,  37 
};
 
static const unsigned char yy_chk[54] = { 0 ,
       1,   1,   1,   1,   1,   1,   1,   1,   3,   3,
       3,   3,   5,   5,  38,  36,   5,   7,   7,   7,
       7,   7,   7,   7,   7,  12,  12,  12,  13,  13,
      14,  14,  20,  20,  23,  23,  27,  27,  34,  32,
      25,  18,  16,   9,  37,  37,  37,  37,  37,  37,
      37,  37,  37 
};
/* <<<<<<<<<<<<<<<<<<<<<<< Auto generate code begin <<<<<<<<<<<<<<<<<<<<<<<*/

#define BEGIN(x) (start = (x))
//#define ECHO fwrite(text, 1, text_end - text, stdout)
#define ECHO
#define CONCAT *comment = *comment + std::string(reinterpret_cast<char*>(text), text_end - text)

void GetComment(unsigned char* input, unsigned char* input_end, std::string* comment) {
  unsigned char *text, *text_end = input, *text_backup = NULL;
  int curr, backup = 0, yyc, action, quote = 0, start = INITIAL;

  while (text_end < input_end) {
    curr = start;
    text = text_end;

    while (1) {
      if (text_end >= input_end) /* EOB */
        break;
      yyc = yy_ec[*text_end];

      if (yy_accept[curr]) {
        backup = curr;
        text_backup = text_end;
      }

      while (yy_chk[yy_base[curr] + yyc] != curr) {
        if ((curr = yy_def[curr]) >= YY_LASTDFA + 2) yyc = yy_meta[yyc];
      }
      curr = yy_nxt[yy_base[curr] + yyc];
      ++text_end;

      if (yy_base[curr] == YY_JAMBASE) break;
    }

    action = yy_accept[curr];
    if (0 == action) {
      text_end = text_backup;
      action = yy_accept[backup];
    }

    switch (action) {
      case XA_MCOMM_BEGIN:
        ECHO;
        CONCAT;
        BEGIN(XS_MCOMM);
        break;
      case XA_SCOMM_BEGIN:
        ECHO;
        CONCAT;
        BEGIN(XS_SCOMM);
        break;
      case XA_QUOTE_BEGIN:
        quote = text[0];
        BEGIN(XS_QUOTE);
        break;
      case XA_MCOMM_END:
        ECHO;
        CONCAT;
        BEGIN(INITIAL);
        break;
      case XA_SCOMM_END:
        ECHO;
        CONCAT;
        BEGIN(INITIAL);
        break;
      case XA_QUOTE_END:
        if (text[0] == quote) BEGIN(INITIAL);
        break;
      case XA_QUOTE_NL:
        ECHO;
        CONCAT;
        fprintf(stderr, "Unexpected EOL in string!\n");
        break;
      default:
        if (start == XS_MCOMM || start == XS_SCOMM) {
          ECHO;
          CONCAT;
        }
        break;
    }
  }
}

std::string g_std_comments;

std::unordered_map<int, std::string> g_remark = {
  {30, "Good!"},
  {25, "Decent!"},
  {15, "Insufficient explanation!"},
};

int CalculateScore(const std::string& comment) {
  std::vector<std::string> lines;
  prism::StringHelper::Split(comment.c_str(), '\n', lines, false);
  
  int nlines = 0;
  int nbytes = 0;
  for (const auto& line : lines) {
    if (!strcasestr(g_std_comments.c_str(), line.c_str())) {
      nlines++;
      nbytes += line.length();
    }
  }
  // printf("%d %d\n", nlines, nbytes);
  // here we get some magic numbers
  if (nbytes >= 1500 || nlines >= 20) return 30;

  if (nbytes >= 900 || nlines >= 12) return 30;
  if (nbytes < 300 || nlines < 5) return 15;
  return 25;
}


#define MAX_FILE_SIZE (16 * 1024 * 1024)

std::string GetCommentFromFile(const char* fname) {
    unsigned char* buffer = new unsigned char[MAX_FILE_SIZE + 4];
    FILE* fp = fopen(fname, "rb");
    std::string comment;
    if (NULL != fp) {
      size_t size = fread(buffer, 1, MAX_FILE_SIZE, fp);
      memset(buffer + size, 0, 4);
      if (size != 0) {
        GetComment(buffer, buffer + size, &comment);
      }
      fclose(fp);
    }
    return comment;
    delete[] buffer;
}

int main(int argc, char* argv[]) {
  g_std_comments = GetCommentFromFile("./myshell_solution.c");
  for (int i = 1; i < argc; ++i) {
    std::string comment = GetCommentFromFile(argv[i]);
    int score = CalculateScore(comment);
    fputs("Item 1 – Explanation of process_cmd (30%)\n", stdout);
    fprintf(stdout, "%d/30  result: %s\n", score, g_remark[score].c_str());
    fprintf(stdout, "Total score = %d/30\n", score);
  }

  return 0;
}
