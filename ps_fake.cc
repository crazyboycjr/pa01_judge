#include <iostream>
#include <string>
int main(int argc, char* argv[]) {
  auto s = R"(    PID TTY          TIME CMD
 163381 pts/1    00:00:02 zsh
 167433 pts/1    00:00:00 ps)";
  auto s2 = R"(USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root           1  0.0  0.0 196684  8620 ?        Ss   Oct21   2:44 /sbin/init
root           2  0.0  0.0      0     0 ?        S    Oct21   0:00 [kthreadd]
root           3  0.0  0.0      0     0 ?        I<   Oct21   0:00 [rcu_gp]
root           4  0.0  0.0      0     0 ?        I<   Oct21   0:00 [rcu_par_gp]
root           6  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kworker/0:0H-kblockd]
root           8  0.0  0.0      0     0 ?        I<   Oct21   0:00 [mm_percpu_wq]
root           9  0.0  0.0      0     0 ?        S    Oct21   0:10 [ksoftirqd/0]
root          10  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcuc/0]
root          11  0.0  0.0      0     0 ?        I    Oct21   1:32 [rcu_preempt]
root          12  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcub/0]
root          13  0.0  0.0      0     0 ?        S    Oct21   0:00 [migration/0]
root          14  0.0  0.0      0     0 ?        S    Oct21   0:00 [idle_inject/0]
root          16  0.0  0.0      0     0 ?        S    Oct21   0:00 [cpuhp/0]
root          17  0.0  0.0      0     0 ?        S    Oct21   0:00 [cpuhp/1]
root          18  0.0  0.0      0     0 ?        S    Oct21   0:00 [idle_inject/1]
root          19  0.0  0.0      0     0 ?        S    Oct21   0:00 [migration/1]
root          20  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcuc/1]
root          21  0.0  0.0      0     0 ?        S    Oct21   0:12 [ksoftirqd/1]
root          23  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kworker/1:0H-kblockd]
root          24  0.0  0.0      0     0 ?        S    Oct21   0:00 [cpuhp/2]
root          25  0.0  0.0      0     0 ?        S    Oct21   0:00 [idle_inject/2]
root          26  0.0  0.0      0     0 ?        S    Oct21   0:00 [migration/2]
root          27  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcuc/2]
root          28  0.0  0.0      0     0 ?        S    Oct21   0:11 [ksoftirqd/2]
root          30  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kworker/2:0H-kblockd]
root          31  0.0  0.0      0     0 ?        S    Oct21   0:00 [cpuhp/3]
root          32  0.0  0.0      0     0 ?        S    Oct21   0:00 [idle_inject/3]
root          33  0.0  0.0      0     0 ?        S    Oct21   0:00 [migration/3]
root          34  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcuc/3]
root          35  0.0  0.0      0     0 ?        S    Oct21   0:11 [ksoftirqd/3]
root          37  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kworker/3:0H-kblockd]
root          38  0.0  0.0      0     0 ?        S    Oct21   0:00 [cpuhp/4]
root          39  0.0  0.0      0     0 ?        S    Oct21   0:00 [idle_inject/4]
root          40  0.0  0.0      0     0 ?        S    Oct21   0:00 [migration/4]
root          41  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcuc/4]
root          42  0.0  0.0      0     0 ?        S    Oct21   0:08 [ksoftirqd/4]
root          44  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kworker/4:0H-kblockd]
root          45  0.0  0.0      0     0 ?        S    Oct21   0:00 [cpuhp/5]
root          46  0.0  0.0      0     0 ?        S    Oct21   0:00 [idle_inject/5]
root          47  0.0  0.0      0     0 ?        S    Oct21   0:00 [migration/5]
root          48  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcuc/5]
root          49  0.0  0.0      0     0 ?        S    Oct21   0:09 [ksoftirqd/5]
root          51  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kworker/5:0H-kblockd]
root          52  0.0  0.0      0     0 ?        S    Oct21   0:00 [cpuhp/6]
root          53  0.0  0.0      0     0 ?        S    Oct21   0:00 [idle_inject/6]
root          54  0.0  0.0      0     0 ?        S    Oct21   0:00 [migration/6]
root          55  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcuc/6]
root          56  0.0  0.0      0     0 ?        S    Oct21   0:10 [ksoftirqd/6]
root          58  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kworker/6:0H-kblockd]
root          59  0.0  0.0      0     0 ?        S    Oct21   0:00 [cpuhp/7]
root          60  0.0  0.0      0     0 ?        S    Oct21   0:00 [idle_inject/7]
root          61  0.0  0.0      0     0 ?        S    Oct21   0:00 [migration/7]
root          62  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcuc/7]
root          63  0.0  0.0      0     0 ?        S    Oct21   0:09 [ksoftirqd/7]
root          65  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kworker/7:0H-kblockd]
root          66  0.0  0.0      0     0 ?        S    Oct21   0:00 [kdevtmpfs]
root          67  0.0  0.0      0     0 ?        I<   Oct21   0:00 [netns]
root          68  0.0  0.0      0     0 ?        S    Oct21   0:00 [rcu_tasks_kthre]
root          69  0.0  0.0      0     0 ?        S    Oct21   0:00 [kauditd]
root          72  0.0  0.0      0     0 ?        S    Oct21   0:00 [khungtaskd]
root          73  0.0  0.0      0     0 ?        S    Oct21   0:00 [oom_reaper]
root          74  0.0  0.0      0     0 ?        I<   Oct21   0:00 [writeback]
root          75  0.0  0.0      0     0 ?        S    Oct21   0:03 [kcompactd0]
root          76  0.0  0.0      0     0 ?        SN   Oct21   0:00 [ksmd]
root          77  0.0  0.0      0     0 ?        SN   Oct21   0:00 [khugepaged]
root         166  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kintegrityd]
root         167  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kblockd]
root         168  0.0  0.0      0     0 ?        I<   Oct21   0:00 [blkcg_punt_bio]
root         172  0.0  0.0      0     0 ?        I<   Oct21   0:00 [edac-poller]
root         174  0.0  0.0      0     0 ?        I<   Oct21   0:00 [devfreq_wq]
root         175  0.0  0.0      0     0 ?        S    Oct21   0:00 [watchdogd]
root         177  0.0  0.0      0     0 ?        S    Oct21   0:53 [kswapd0]
root         181  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kthrotld]
root         184  0.0  0.0      0     0 ?        I<   Oct21   0:00 [acpi_thermal_pm]
root         186  0.0  0.0      0     0 ?        I<   Oct21   0:00 [nvme-wq]
root         187  0.0  0.0      0     0 ?        I<   Oct21   0:00 [nvme-reset-wq]
root         188  0.0  0.0      0     0 ?        I<   Oct21   0:00 [nvme-delete-wq]
root         190  0.0  0.0      0     0 ?        I<   Oct21   0:00 [ipv6_addrconf]
root         202  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kstrp]
root         218  0.0  0.0      0     0 ?        I<   Oct21   0:00 [charger_manager]
root         266  0.0  0.0      0     0 ?        I<   Oct21   0:00 [ata_sff]
root         267  0.0  0.0      0     0 ?        S    Oct21   0:00 [scsi_eh_0]
root         268  0.0  0.0      0     0 ?        I<   Oct21   0:00 [scsi_tmf_0]
root         269  0.0  0.0      0     0 ?        S    Oct21   0:00 [scsi_eh_1]
root         271  0.0  0.0      0     0 ?        I<   Oct21   0:00 [scsi_tmf_1]
root         282  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-worker]
root         283  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-worker-hi]
root         284  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-delalloc]
root         285  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-flush_del]
root         286  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-cache]
root         287  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-submit]
root         288  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-fixup]
root         289  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-endio]
root         290  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-endio-met]
root         291  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-endio-met]
root         292  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-endio-rai]
root         293  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-endio-rep]
root         294  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-rmw]
root         295  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-endio-wri]
root         296  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-freespace]
root         297  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-delayed-m]
root         298  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-readahead]
root         299  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-qgroup-re]
root         300  0.0  0.0      0     0 ?        I<   Oct21   0:00 [btrfs-extent-re]
root         301  0.0  0.0      0     0 ?        S    Oct21   0:12 [btrfs-cleaner]
root         302  0.0  0.0      0     0 ?        S    Oct21   3:35 [btrfs-transacti]
root         309  0.0  0.0      0     0 ?        I<   Oct21   0:02 [kworker/7:1H-kblockd]
root         310  0.0  0.0      0     0 ?        I<   Oct21   0:02 [kworker/1:1H-kblockd]
root         312  0.0  0.0      0     0 ?        I<   Oct21   0:02 [kworker/2:1H-kblockd]
root         315  0.0  0.0      0     0 ?        I<   Oct21   0:02 [kworker/5:1H-kblockd]
root         319  0.0  0.0      0     0 ?        I<   Oct21   0:02 [kworker/6:1H-kblockd]
root         323  0.0  0.0      0     0 ?        I<   Oct21   0:02 [kworker/0:1H-kblockd]
root         340  0.4  1.1 359328 190340 ?       Ss   Oct21  28:42 /usr/lib/systemd/systemd-journald
root         347  0.0  0.0  78124  1044 ?        Ss   Oct21   0:00 /usr/bin/lvmetad -f
root         352  0.0  0.0      0     0 ?        I<   Oct21   0:02 [kworker/4:1H-kblockd]
root         355  0.0  0.0  41272  7104 ?        Ss   Oct21   0:02 /usr/lib/systemd/systemd-udevd
root         358  0.0  0.0      0     0 ?        I<   Oct21   0:02 [kworker/3:1H-kblockd]
root         397  0.0  0.0      0     0 ?        I<   Oct21   0:00 [kmemstick]
root         454  0.0  0.0      0     0 ?        I<   Oct21   0:00 [cfg80211]
root         475  0.0  0.0      0     0 ?        I<   Oct21   0:00 [cryptd]
systemd+     525  0.0  0.0 100008  4292 ?        Ssl  Oct21   0:00 /usr/lib/systemd/systemd-timesyncd
root         532  0.0  0.0      0     0 ?        I<   Oct21   0:00 [rpciod]
root         533  0.0  0.0      0     0 ?        I<   Oct21   0:00 [xprtiod]
root         542  0.0  0.0   2352  1800 ?        Ss   Oct21   3:06 /usr/bin/acpid --foreground --netlink
avahi        543  0.0  0.0  10592  3660 ?        Ss   Oct21   0:00 avahi-daemon: running [cjr-XPS.local]
root         545  0.0  0.0   5736  3316 ?        Ss   Oct21   0:00 /usr/bin/bumblebeed
dbus         546  0.0  0.0  13412  5764 ?        Ss   Oct21   1:29 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
root         548  0.0  0.1 562000 17080 ?        Ssl  Oct21   0:54 /usr/bin/NetworkManager --no-daemon
root         552  0.0  0.0   9964  3756 ?        Ss   Oct21   0:52 /usr/bin/tincd -n vpn0 -D
avahi        555  0.0  0.0  10468   344 ?        S    Oct21   0:00 avahi-daemon: chroot helper
root         569  0.0  0.0  50072  9272 ?        Ss   Oct21   0:00 /usr/bin/cupsd -l
nobody       572  0.0  0.1  44392 21868 ?        Ss   Oct21   0:02 /usr/bin/python /usr/bin/sslocal -c /etc/shadowsocks/config_linode1.json
nobody       573  0.0  0.1  45188 22456 ?        Ss   Oct21   0:41 /usr/bin/python /usr/bin/sslocal -c /etc/shadowsocks/config_linode2.json
nobody       574  0.0  0.1  44392 21908 ?        Ss   Oct21   0:02 /usr/bin/python /usr/bin/sslocal -c /etc/shadowsocks/config_linode2_v6.json
nobody       575  0.0  0.1  44392 21996 ?        Ss   Oct21   0:02 /usr/bin/python /usr/bin/sslocal -c /etc/shadowsocks/homecn2.json
root         576  0.0  0.0  12544  4108 ?        Ss   Oct21   0:00 /usr/bin/sshd -D
root         582  0.0  0.0 1511072 7916 ?        Sl   Oct21   4:32 /opt/teamviewer/tv_bin/teamviewerd -d
root         608  0.0  0.0  60660  1892 ?        Ssl  Oct21   0:00 /usr/bin/gssproxy -D
root         633  0.0  0.0 249832  7248 ?        Ssl  Oct21   0:00 /usr/bin/gdm
colord       640  0.0  0.0 258944 13424 ?        Ssl  Oct21   0:00 /usr/lib/colord
mysql        645  0.0  0.4 1045372 66180 ?       Ssl  Oct21   1:48 /usr/bin/mysqld
root         649  0.0  0.0  26504  5844 ?        Ss   Oct21   0:01 /usr/lib/systemd/systemd-logind
root         669  0.0  0.0  13400  5820 ?        Ss   Oct21   0:01 /usr/bin/wpa_supplicant -u
root         670  0.0  0.0 249688  7080 ?        Ssl  Oct21   0:00 /usr/lib/accounts-daemon
polkitd      675  0.0  0.1 1935200 22956 ?       Ssl  Oct21   0:04 /usr/lib/polkit-1/polkitd --no-debug
rtkit        828  0.0  0.0 154676  2656 ?        SNsl Oct21   0:02 /usr/lib/rtkit-daemon
root         846  0.0  0.0      0     0 ?        S<   Oct21   0:00 [krfcommd]
root         889  0.0  0.0 265420  9252 ?        Ssl  Oct21   0:40 /usr/lib/upowerd
root        1046  0.0  0.0 363800 10016 ?        Sl   Oct21   0:00 gdm-session-worker [pam/gdm-password]
cjr         1053  0.0  0.0  32812  7124 ?        Ss   Oct21   0:01 /usr/lib/systemd/systemd --user
cjr         1054  0.0  0.0  74252  3168 ?        S    Oct21   0:00 (sd-pam)
cjr         1062  0.0  0.0 398404  7180 ?        Sl   Oct21   0:01 /usr/bin/gnome-keyring-daemon --daemonize --login
cjr         1066  0.0  0.0 174712  5496 tty2     Ssl+ Oct21   0:00 /usr/lib/gdm-x-session --run-script /usr/bin/gnome-session
cjr         1068  1.2  1.2 628300 199272 tty2    Sl+  Oct21  76:45 /usr/lib/Xorg vt2 -displayfd 3 -auth /run/user/1000/gdm/Xauthority -nolisten tcp -background none -noreset -keeptty -verbose 3
root        1072  0.0  0.0   2304   640 tty2     S+   Oct21   0:00 xf86-video-intel-backlight-helper intel_backlight
cjr         1075  0.0  0.0  12284  5524 ?        Ss   Oct21   2:37 /usr/bin/dbus-daemon --session --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
cjr         1077  0.0  0.0 276000  8224 tty2     Sl+  Oct21   0:00 /usr/lib/gnome-session-binary
cjr         1080  0.0  0.0   4568  1996 ?        Ss   Oct21   0:19 syndaemon -i 0.5 -t -K -R -d
cjr         1097  0.0  0.0 306288  5908 ?        Ssl  Oct21   0:00 /usr/lib/at-spi-bus-launcher
cjr         1103  0.0  0.0  10756  3924 ?        S    Oct21   0:07 /usr/bin/dbus-daemon --config-file=/usr/share/defaults/at-spi2/accessibility.conf --nofork --print-address 3
cjr         1109  0.0  0.0 100752  3880 ?        Ssl  Oct21   0:00 /usr/lib/gnome-session-ctl --monitor
cjr         1110  0.2  0.0 1828692 13548 ?       S<sl Oct21  17:15 /usr/bin/pulseaudio --daemonize=no
cjr         1112  0.0  0.0 498036 10084 ?        Ssl  Oct21   0:03 /usr/lib/gnome-session-binary --systemd-service --session=gnome
cjr         1126  2.5  5.0 4269956 821360 ?      Ssl  Oct21 160:48 /usr/bin/gnome-shell
cjr         1137  0.0  0.0 243488  5896 ?        Sl   Oct21   0:00 /usr/lib/pulse/gsettings-helper
cjr         1154  0.0  0.0 248424  6484 ?        Ssl  Oct21   0:00 /usr/lib/gvfsd
cjr         1159  0.0  0.0 380932  5176 ?        Sl   Oct21   0:00 /usr/lib/gvfsd-fuse /run/user/1000/gvfs -f
cjr         1184  0.0  0.0 163732  6624 ?        Sl   Oct21   0:42 /usr/lib/at-spi2-registryd --use-gnome-session
cjr         1188  0.0  0.0 516004 12028 ?        Sl   Oct21   0:00 /usr/lib/gnome-shell-calendar-server
cjr         1192  0.0  0.0 1091912 15884 ?       Ssl  Oct21   0:00 /usr/lib/evolution-source-registry
cjr         1199  0.0  0.0 559252 14908 ?        Sl   Oct21   0:00 /usr/lib/goa-daemon
cjr         1201  0.0  0.0 157824  5836 ?        Sl   Oct21   0:02 /usr/lib/dconf-service
cjr         1207  0.0  0.0 321444  9912 ?        Sl   Oct21   0:16 /usr/lib/telepathy/mission-control-5
cjr         1217  0.0  0.0 323416  6856 ?        Sl   Oct21   0:00 /usr/lib/goa-identity-service
cjr         1218  0.0  0.0 361192  8248 ?        Ssl  Oct21   0:00 /usr/lib/gvfs-udisks2-volume-monitor
root        1224  0.0  0.0 404528 10908 ?        Ssl  Oct21   1:46 /usr/lib/udisks2/udisksd
cjr         1233  0.0  0.0 244640  4892 ?        Ssl  Oct21   0:00 /usr/lib/gvfs-mtp-volume-monitor
cjr         1276  0.0  0.0 319580  5868 ?        Sl   Oct21   0:01 ibus-daemon --panel disable -r --xim
cjr         1281  0.0  0.0 245264  5392 ?        Sl   Oct21   0:00 /usr/lib/ibus/ibus-dconf
cjr         1282  0.0  0.1 289180 20176 ?        Sl   Oct21   0:05 /usr/lib/ibus/ibus-extension-gtk3
cjr         1286  0.0  0.0 211748 16120 ?        Sl   Oct21   0:04 /usr/lib/ibus/ibus-x11 --kill-daemon
cjr         1289  0.0  0.0 245328  5780 ?        Sl   Oct21   0:01 /usr/lib/ibus/ibus-portal
cjr         1296  0.0  0.0 322528  6796 ?        Sl   Oct21   0:00 /usr/lib/gvfsd-trash --spawner :1.22 /org/gtk/gvfs/exec_spaw/0
cjr         1310  0.0  0.1 1008560 17680 ?       Ssl  Oct21   0:01 /usr/lib/evolution-calendar-factory
cjr         1323  0.0  0.0 759048 13904 ?        Ssl  Oct21   0:00 /usr/lib/evolution-addressbook-factory
cjr         1332  0.0  0.0 318392  5092 ?        Ssl  Oct21   0:00 /usr/lib/gsd-a11y-settings
cjr         1333  0.0  0.1 929852 18692 ?        Ssl  Oct21   0:06 /usr/lib/gsd-color
cjr         1334  0.0  0.0 388568  9760 ?        Ssl  Oct21   0:00 /usr/lib/gsd-datetime
cjr         1335  0.0  0.0 321916  7440 ?        Ssl  Oct21   0:14 /usr/lib/gsd-housekeeping
cjr         1338  0.0  0.1 359164 16284 ?        Ssl  Oct21   0:05 /usr/lib/gsd-keyboard
cjr         1340  0.0  0.1 1113112 23704 ?       Ssl  Oct21   0:13 /usr/lib/gsd-media-keys
cjr         1341  0.0  0.1 700688 19316 ?        Ssl  Oct21   0:09 /usr/lib/gsd-power
cjr         1342  0.0  0.0 261096  9816 ?        Ssl  Oct21   0:00 /usr/lib/gsd-print-notifications
cjr         1344  0.0  0.0 465804  5108 ?        Ssl  Oct21   0:00 /usr/lib/gsd-rfkill
cjr         1348  0.0  0.0 244240  5316 ?        Ssl  Oct21   0:00 /usr/lib/gsd-screensaver-proxy
cjr         1349  0.0  0.0 470332  9680 ?        Ssl  Oct21   0:15 /usr/lib/gsd-sharing
cjr         1352  0.0  0.0 323524  5868 ?        Ssl  Oct21   0:00 /usr/lib/gsd-smartcard
cjr         1354  0.0  0.0 331632  6536 ?        Ssl  Oct21   0:00 /usr/lib/gsd-sound
cjr         1357  0.0  0.1 285524 16412 ?        Ssl  Oct21   0:05 /usr/lib/gsd-wacom
cjr         1360  0.0  0.0 322624  5848 ?        Ssl  Oct21   0:00 /usr/lib/gsd-wwan
cjr         1361  0.0  0.1 359624 17464 ?        Ssl  Oct21   0:07 /usr/lib/gsd-xsettings
cjr         1366  0.0  0.1 535216 29272 ?        Sl   Oct21   0:51 /usr/lib/kdeconnectd
cjr         1383  0.0  0.1 673420 28452 ?        Sl   Oct21   0:05 /usr/lib/evolution-data-server/evolution-alarm-notify
cjr         1397  0.0  0.0  27184 11888 ?        S    Oct21   0:02 python3 /usr/bin/libinput-gestures
cjr         1413  0.0  0.2 580656 44384 ?        Sl   Oct21   4:26 /usr/bin/tilda
cjr         1417  0.0  2.1 921144 344464 ?       SNl  Oct21   0:40 /usr/lib/tracker-miner-fs
cjr         1453  0.0  0.0 171400  5000 ?        Sl   Oct21   0:00 /usr/lib/ibus/ibus-engine-simple
cjr         1503  0.0  0.0  30012  4372 ?        S    Oct21   0:00 /usr/lib/gconfd-2
cjr         1513  0.0  0.0 358184  9448 ?        Sl   Oct21   0:00 /usr/lib/gsd-printer
cjr         1530  0.0  0.6 3251872 103256 ?      Sl   Oct21   5:44 sogou-qimpanel
cjr         1570  0.0  0.0 543992  7072 ?        Sl   Oct21   0:00 /usr/lib/gvfsd-network --spawner :1.22 /org/gtk/gvfs/exec_spaw/1
cjr         1600  0.0  0.0   8820  3180 ?        S    Oct21   0:03 libinput-debug-events --device /dev/input/event18
cjr         1665  0.0  0.5 239528 84700 ?        Sl   Oct21   4:46 fcitx
cjr         1670  0.0  0.0  10912  3160 ?        Ss   Oct21   1:17 /usr/bin/dbus-daemon --syslog --fork --print-pid 4 --print-address 6 --config-file /usr/share/fcitx/dbus/daemon.conf
cjr         1677  0.0  0.0   7176   212 ?        SN   Oct21   0:00 /usr/bin/fcitx-dbus-watcher unix:abstract=/tmp/dbus-NAloA9yYid,guid=944cc1aa1c91683b727e36765dad22ed 1670
cjr         1686  0.0  0.1 388436 20984 ?        S    Oct21   0:44 sogou-qimpanel-watchdog
cjr         1827  0.0  0.0      0     0 ?        Z    Oct21   0:00 [sh] <defunct>
cjr         1909  0.0  0.0 325516  6796 ?        Sl   Oct21   0:00 /usr/lib/gvfsd-dnssd --spawner :1.22 /org/gtk/gvfs/exec_spaw/3
cjr         1976  0.0  0.0  15896   400 ?        S    Oct21   0:00 cat
cjr         1977  0.0  0.0  15896   392 ?        S    Oct21   0:00 cat
cjr         3343  0.0  0.0  45252  5404 ?        Ss   Oct21   0:00 /usr/lib/bluetooth/obexd
root        3374  0.0  0.0  12800  5492 ?        Ss   Oct21   0:14 /usr/lib/bluetooth/bluetoothd
cjr         6830  0.0  0.0 167204  2460 ?        SLs  Oct21   0:05 /usr/bin/gpg-agent --supervised
cjr        10941  0.0  0.1 629004 26812 ?        SLl  Oct21   0:05 /usr/bin/seahorse --gapplication-service
cjr        16748  0.0  0.0 174812  8888 ?        Ssl  Oct21   0:00 /usr/lib/gvfsd-metadata
cjr        20877  0.0  0.0   5796  2144 ?        S    Oct21   0:00 /usr/bin/ssh-agent -D -a /run/user/1000/keyring/.ssh
cjr        61830  0.0  1.1 5328240 194152 ?      Ssl  Oct22   2:11 /usr/bin/dropbox
cjr        78003  0.5  3.7 3836424 614020 ?      Sl   Oct23  20:14 telegram-desktop --
cjr        99827  0.0  0.0 291684  9104 ?        Sl   Oct23   0:00 /usr/lib/gvfsd-http --spawner :1.22 /org/gtk/gvfs/exec_spaw/4
cjr       105440  0.0  0.1  43236 18896 ?        S    Oct23   0:03 /usr/bin/python /usr/bin/powerline-daemon -q
cjr       121722  0.0  0.9 1262212 162112 ?      Sl   Oct24   0:47 nautilus
cjr       123260  0.0  0.0  23516  8576 pts/0    Ss   Oct24   0:24 /bin/zsh
root      127156  0.0  0.0      0     0 ?        S    Oct24   0:22 [irq/128-brcmf_p]
root      127157  0.0  0.0      0     0 ?        I<   Oct24   0:00 [msgbuf_txflow]
cjr       140419  0.0  1.3 882100 218204 ?       Sl   Oct24   0:38 okular /home/cjr/Documents/pg_year2_1/comp3511/pa01_judge/comp3511_pa1_grading_plan.pdf
cjr       150807  0.0  0.3 498488 53016 ?        Sl   Oct24   0:20 tilda
cjr       150814  0.0  0.0  23308  8924 pts/2    Ss   Oct24   0:02 /bin/zsh
cjr       153873  0.6  3.4 1407396 567548 ?      SLl  Oct24   8:35 /opt/google/chrome/chrome
cjr       153878  0.0  0.0  15896   768 ?        S    Oct24   0:00 cat
cjr       153879  0.0  0.0  15896   708 ?        S    Oct24   0:00 cat
cjr       153889  0.0  0.3 250680 49488 ?        S    Oct24   0:00 /opt/google/chrome/chrome --type=zygote
cjr       153890  0.0  0.0  16668  4928 ?        S    Oct24   0:00 /opt/google/chrome/nacl_helper
cjr       153893  0.0  0.0 250680 13628 ?        S    Oct24   0:00 /opt/google/chrome/chrome --type=zygote
cjr       153913  2.0  0.8 428008 142820 ?       Sl   Oct24  25:09 /opt/google/chrome/chrome --type=gpu-process --field-trial-handle=10862420698693865150,7914391238595743898,131072 --gpu-preferences=KAAAAAAAAAAg
cjr       153918  0.1  0.7 464732 114852 ?       SLl  Oct24   1:58 /opt/google/chrome/chrome --type=utility --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --service-sandbox-typ
cjr       154012  0.0  0.5 555884 92716 ?        Sl   Oct24   0:06 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154064  0.0  0.4 536684 77424 ?        Sl   Oct24   0:00 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154116  0.0  0.5 546156 88468 ?        Sl   Oct24   0:02 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154141  0.0  0.7 655276 125524 ?       Sl   Oct24   0:05 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154160  0.0  0.5 550764 91000 ?        Sl   Oct24   0:02 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154172  0.0  0.6 569272 104648 ?       Sl   Oct24   0:03 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154184  0.0  0.5 557824 92808 ?        Sl   Oct24   0:00 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154189  0.0  0.6 573184 105764 ?       Sl   Oct24   0:04 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154208  0.0  0.4 542060 80276 ?        Sl   Oct24   0:00 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154220  0.0  0.4 536684 77984 ?        Sl   Oct24   0:00 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154225  0.0  0.6 590876 105928 ?       Sl   Oct24   0:17 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154237  0.0  0.4 536684 78160 ?        Sl   Oct24   0:00 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       154310  0.0  0.8 594008 138592 ?       Sl   Oct24   0:04 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154376  0.0  0.9 628124 157340 ?       Sl   Oct24   0:05 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154381  0.0  0.9 607624 153356 ?       Sl   Oct24   0:05 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154406  0.0  0.9 604988 148992 ?       Sl   Oct24   0:05 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154423  0.1  0.5 561716 96860 ?        Sl   Oct24   2:20 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154587  0.0  0.9 606788 149828 ?       Sl   Oct24   0:05 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154646  0.0  0.5 543656 87360 ?        Sl   Oct24   0:03 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154667  0.0  0.9 620832 156724 ?       Sl   Oct24   0:05 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154679  0.0  0.9 612896 159124 ?       Sl   Oct24   0:05 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154696  0.0  1.4 764816 234932 ?       Sl   Oct24   1:05 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154743  0.0  1.0 669588 175048 ?       Sl   Oct24   0:11 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154749  0.0  1.0 651456 162492 ?       Sl   Oct24   0:06 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154792  0.0  1.3 718888 212144 ?       Sl   Oct24   0:25 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154809  0.0  0.9 629388 151160 ?       Sl   Oct24   0:09 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154815  0.0  0.9 649152 157380 ?       Sl   Oct24   0:19 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154857  0.0  0.8 623104 144120 ?       Sl   Oct24   0:36 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154888  0.0  0.9 630668 151744 ?       Sl   Oct24   0:09 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       154901  0.0  1.0 659160 176756 ?       Sl   Oct24   0:11 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       155343  0.0  1.0 640416 165164 ?       Sl   Oct24   0:24 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       155376  0.0  0.7 632440 116924 ?       Sl   Oct24   0:04 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
root      159812  0.0  0.0      0     0 ?        I    01:19   0:01 [kworker/u16:2-btrfs-endio-write]
cjr       163319  0.0  0.9 627424 147320 ?       Sl   02:14   0:04 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       163374  0.0  0.3 500596 54644 ?        Sl   02:15   0:30 tilda
cjr       163381  0.0  0.0  23128  8772 pts/1    Ss   02:15   0:02 /bin/zsh
cjr       163811  0.0  1.0 668312 172260 ?       Sl   02:22   0:05 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
cjr       163824  0.1  1.1 671768 180572 ?       Sl   02:22   1:07 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
root      163839  0.0  0.0      0     0 ?        I    02:22   0:00 [kworker/7:0-events]
cjr       163869  0.0  0.7 621188 123712 ?       Sl   02:22   0:34 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --enable-auto-reload
root      164374  0.0  0.0      0     0 ?        I    02:30   0:00 [kworker/1:0-events]
cjr       164381  0.0  0.3 1466564 58768 ?       Sl   02:30   0:00 /usr/bin/gnome-calendar --gapplication-service
root      164417  0.0  0.0      0     0 ?        I    02:30   0:00 [kworker/5:2-mm_percpu_wq]
root      164978  0.0  0.0      0     0 ?        I    02:39   0:00 [kworker/0:2-events]
root      165199  0.0  0.0      0     0 ?        I<   02:45   0:00 [kworker/u17:1-btrfs-worker-high]
root      165550  0.0  0.0      0     0 ?        I    02:49   0:00 [kworker/6:1-events]
root      165640  0.0  0.0      0     0 ?        I    02:52   0:00 [kworker/2:1-events]
root      165818  0.0  0.0      0     0 ?        I    02:54   0:00 [kworker/3:0-mm_percpu_wq]
root      165920  0.0  0.0      0     0 ?        I    02:58   0:00 [kworker/4:3-events]
root      165998  0.0  0.0      0     0 ?        I    03:00   0:00 [kworker/u16:18-btrfs-submit]
root      166028  0.0  0.0      0     0 ?        I    19:59   0:00 [kworker/u16:48-btrfs-delalloc]
root      166047  0.0  0.0      0     0 ?        I    19:59   0:00 [kworker/u16:67-btrfs-endio-write]
root      166048  0.0  0.0      0     0 ?        I    19:59   0:00 [kworker/u16:68-btrfs-submit]
root      166052  0.0  0.0      0     0 ?        I    19:59   0:00 [kworker/u16:72-events_unbound]
root      166053  0.0  0.0      0     0 ?        I    19:59   0:00 [kworker/u16:73-btrfs-endio-write]
root      166056  0.0  0.0      0     0 ?        S    19:59   0:00 [irq/129-mei_me]
root      166057  0.0  0.0      0     0 ?        I    19:59   0:00 [kworker/6:2-events]
root      166065  0.0  0.0      0     0 ?        I    19:59   0:00 [kworker/2:0-events]
root      166078  0.0  0.0      0     0 ?        I    19:59   0:00 [kworker/3:1-events]
root      166113  0.0  0.0      0     0 ?        S<   19:59   0:00 [khidpd_046db342]
cjr       166223  0.0  0.5 545136 89140 ?        Sl   20:00   0:00 /opt/google/chrome/chrome --type=renderer --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --extension-process 
cjr       166261  0.0  0.2 221036 37608 ?        Sl   20:00   0:00 /usr/bin/python /usr/bin/chrome-gnome-shell chrome-extension://gphhapmejobijbbhgpjhcjognlahblep/
root      166516  0.0  0.0      0     0 ?        I    20:07   0:00 [kworker/1:1-events]
root      166535  0.0  0.0      0     0 ?        I<   20:07   0:00 [kworker/u17:2-btrfs-worker-high]
cjr       166629  0.0  0.1  71404 20124 pts/2    S+   20:09   0:00 vim /home/cjr/Developing/socket_code/client.cc
root      166639  0.0  0.0      0     0 ?        I    20:09   0:00 [kworker/5:0-events]
cjr       166667  0.0  0.1 156100 31256 pts/0    Sl+  20:10   0:00 vim judge.cc
root      166681  0.0  0.0      0     0 ?        I    20:10   0:00 [kworker/4:0-mm_percpu_wq]
root      166689  0.0  0.0      0     0 ?        I    20:10   0:00 [kworker/u16:0-btrfs-submit]
root      166785  0.0  0.0      0     0 ?        I    20:11   0:00 [kworker/0:1-events]
root      166831  0.0  0.0      0     0 ?        I    20:12   0:00 [kworker/7:2-events]
root      166840  0.0  0.0      0     0 ?        I    20:12   0:00 [kworker/u16:1-events_unbound]
root      166841  0.0  0.0      0     0 ?        I    20:12   0:00 [kworker/u16:3-btrfs-endio-write]
cjr       166920  1.2  0.3 561428 54552 ?        Sl   20:13   0:08 /opt/google/chrome/chrome --type=utility --field-trial-handle=10862420698693865150,7914391238595743898,131072 --lang=en-US --service-sandbox-typ
root      167171  0.0  0.0      0     0 ?        I    20:19   0:00 [kworker/0:0-events]
root      167202  0.0  0.0      0     0 ?        I<   20:19   0:00 [kworker/u17:0-hci0]
root      167373  0.0  0.0      0     0 ?        I    20:21   0:00 [kworker/4:1]
root      167409  0.0  0.0      0     0 ?        I    20:22   0:00 [kworker/u16:4-btrfs-endio-write]
root      167422  0.0  0.0      0     0 ?        I    20:23   0:00 [kworker/6:0-events]
root      167423  0.0  0.0      0     0 ?        I    20:23   0:00 [kworker/6:3]
root      167426  0.0  0.0      0     0 ?        I    20:23   0:00 [kworker/3:2-events]
cjr       167436  0.0  0.0  23896  3732 pts/1    R+   20:23   0:00 ps aux)";
  if (argc == 1) {
    std::cout << s << "\n";
  } else {
    std::cout << s2 << "\n";
  }
  return 0;
}
