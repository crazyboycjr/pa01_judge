
#ifndef JSON_HELPER_H_
#define JSON_HELPER_H_

#include "./property_tree.h"

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

class JsonHelper : public PropertyTree {
 public:
  JsonHelper() {}
  JsonHelper(pt::ptree root) : root_{root} {}
  virtual ~JsonHelper() noexcept {}

  pt::ptree& root() { return root_; }

  void FromChild(const pt::ptree& child) {
    root_ = child;
  }

  void Parse(const std::string& json) override {
    std::stringstream ss(json);
    try {
      pt::read_json(ss, root_);
    } catch (std::exception& e) {
      LOG(FATAL) << "cannot parse json: " << e.what();
    }
  }

  std::string Build() const override {
    std::stringstream ss;
    try {
      pt::write_json(ss, root_, false);
    } catch (std::exception& e) {
      LOG(FATAL) << "cannot build json: " << e.what();
    }
    return ss.str();
  }

  pt::ptree GetChild(const std::string& path) const {
    pt::ptree df_node;
    return root_.get_child(path, df_node);
  }

  template <typename T>
  inline T GetValue(const std::string& path) const {
    return root_.get<T>(path);
  }

  template <typename T>
  inline T GetValue(const std::string& path, const T& default_value) const {
    return root_.get<T>(path, default_value);
  }

  template <typename T>
  inline void SetValue(const std::string& key, const T& value) {
    root_.put<T>(key, value);
  }

 private:
  pt::ptree root_;
};

#endif  // JSON_HELPER_H_
