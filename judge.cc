#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#include <iostream>
#include <regex>
#include <chrono>

#include "./logging.h"
#include "./utils.h"

#include "./json_helper.h"

static bool g_byhand = false;

const size_t kLineSize = 1024;
const size_t kBufferSize = 1024 << 10;  /// 1MB buffer
char buffer[kBufferSize];

void Usage(const char* app) {
  fprintf(stderr, "Usage: %s <binary> <test>\n", app);
  fprintf(stderr, "       JUDGE_BYHAND=1 %s <binary>\n", app);
}

int SetNonBlocking(int fd, bool non_block) {
  int flags, ret;
  flags = fcntl(fd, F_GETFL);
  if (non_block) {
    flags |= O_NONBLOCK;
  } else {
    flags &= ~O_NONBLOCK;
  }
  ret = fcntl(fd, F_SETFL, flags);
  return ret;
}


struct Pipe {
  int read_end;
  int write_end;

  void Create(int flags) {
    int fd[2];
    PCHECK(-1 != pipe2(fd, flags));
    read_end = fd[0];
    write_end = fd[1];
  }
};

struct PipePair {
  Pipe sp;  // send pipe
  Pipe rp;  // recv pipe
};

PipePair pp;

class Timer {
 public:
  using Clock = std::chrono::high_resolution_clock;

  Timer(int timeout_ms) : timeout_ms_{timeout_ms} {
  }

  void Tick() {
    last_tp_ = Clock::now();
  }

  bool Timeout() {
    auto now = Clock::now();
    return (now - last_tp_).count() / 1e6 >= timeout_ms_;
  }

 private:
  int timeout_ms_;
  Clock::time_point last_tp_;
};

void WriteBuffer(char* buffer, size_t size) {
  write(1, buffer, size);
}

template <typename CheckFunc>
void CheckOutput(CheckFunc check_func) {
    Timer timer(100);  /// 100ms timer
    timer.Tick();
    size_t size = 0;
    do {
      ssize_t n = read(pp.sp.read_end, buffer + size, kBufferSize - size);
      if (n > 0) {
        size += n;
      } else if (n == -1) {
        PCHECK(errno == EAGAIN);
      }
    } while (!timer.Timeout());
    check_func(buffer, size);
}

class Grader {
 public:
  struct TestCase {
    std::string command;
    std::string expected_output;
    int points;
    bool correct;
    std::string err_desc;
    
    TestCase() {
    }

    TestCase(const JsonHelper& json) {
      command = json.GetValue<std::string>("command");
      expected_output = json.GetValue<std::string>("expected_output");
      points = json.GetValue<int>("points");
      correct = false;
    }
  };

  Grader(const char* input_file)
      : input_file_{input_file},
        cur_cmd_{0},
        total_points_{0},
        pid_{-1},
        pid2_{-2},
        restart_{false} {}

  void LoadTest() {
    std::stringstream ss;
    std::ifstream ifile(input_file_);

    CHECK(ifile) << "failed to open testfile " << input_file_;

    ss << ifile.rdbuf();
    ifile.close();

    json.Parse(ss.str());

    description_ = json.GetValue<std::string>("description");
    num_commands_ = json.GetValue<int>("num_commands");

    auto child = json.GetChild("testcases");
    JsonHelper child_json;

    for (auto ch : child) {
      child_json.FromChild(ch.second);
      testcases_.emplace_back(child_json);
      total_points_ += testcases_.back().points;
    }
  }

  bool Done() { return cur_cmd_ == num_commands_; }

  bool ShouldRestart() {
    auto& tcase = testcases_.at(cur_cmd_ - 1);
    // if (tcase.should_restart)
    // special judge here
    if (tcase.command == "\n") return false;
    return true;
  }

  void Restart() {
    restart_ = true;
    if (cur_cmd_ > 0) cur_cmd_--;
  }

  void Next(char* line) {
    std::string command = testcases_.at(cur_cmd_).command;
    CHECK_LT(command.size(), kLineSize);
    strncpy(line, command.c_str(), command.size());
    line[command.size()] = '\0';
    // printf("line = %s\n", line);

    cur_cmd_++;
  }

  void Check(char* buffer, size_t size) {
    std::string buf_str(buffer, size);

    if (cur_cmd_ == 0 || restart_) {
      // special judge for the initial message
      restart_ = false;

      std::regex start_regex("The shell program \\(pid=([0-9]+)\\) starts.*(\r|\n)*.*");
      std::smatch pid_match;

      if (std::regex_match(buf_str, pid_match, start_regex)) {
        if (pid_match.size() >= 2) {
          // printf("catch init pid = %s\n", pid_match[1].str().c_str());
          pid_ = stoi(pid_match[1].str());
        }
      }
    } else {
      TestCase& tcase = testcases_.at(cur_cmd_ - 1);

      int len = tcase.expected_output.length();
      // printf("%ld %d\n", size, len);
      if (!strncmp(buffer, tcase.expected_output.c_str(), len) ||
          (len > 3 && !strncmp(buffer, tcase.expected_output.c_str(), len - 3))) {
        tcase.correct = true;
      } else {
#ifdef JUDGE_DEBUG
        tcase.err_desc =
            prism::FormatString("output doesn't match: %s v.s. %s\n",
                                buf_str.c_str(), tcase.expected_output.c_str());
#else
        tcase.err_desc =
            prism::FormatString("output doesn't match");
#endif
        //printf("error on command: %s", tcase.command.c_str());
        //LOG(INFO) << buf_str << " vs " << tcase.expected_output;
      }
    }
  }

  void CheckExitMessage(char* buffer, size_t size) {
    std::regex end_regex("The shell program \\(pid=([0-9]+)\\) terminates.*(\r|\n)*.*");
    std::smatch pid_match;

    std::string buf_str(buffer, size);
    TestCase& tcase = testcases_.at(cur_cmd_ - 1);

    if (std::regex_match(buf_str, pid_match, end_regex)) {

      if (pid_match.size() >= 2) {
        // printf("catch end pid = %s\n", pid_match[1].str().c_str());
        int pid = stoi(pid_match[1].str());
        pid2_ = pid;
        if (pid == pid_) {
          tcase.correct = true;
        } else {
          tcase.correct = false;
          tcase.err_desc = prism::FormatString("pid not match %d v.s. %s", pid_, pid);
        }
      }

    }

    if (!tcase.correct) {
      if (tcase.err_desc.length() == 0) {
        tcase.err_desc = prism::FormatString("no exit message or exit message does not match");
      }
    }
  }

  void SetExitCode(int status) {
    TestCase& tcase = testcases_.at(cur_cmd_ - 1);
    if (cur_cmd_ != num_commands_) {
      tcase.correct = false;
      tcase.err_desc = "program exits unexpectedly";
      return;
    }

    int rc;
    if (WIFEXITED(status)) {
      rc = WEXITSTATUS(status);
      if (rc != 0) {
        tcase.correct = false;
        tcase.err_desc = prism::FormatString("exit with error code = %d", rc);
        //printf("exit with error code = %d\n", rc);
      }
    } else if (WIFSIGNALED(status)) {
      rc = WTERMSIG(status);
      tcase.correct = false;
      tcase.err_desc = prism::FormatString("program interupted by signal, signal number = %d", rc);
      //printf("program interupted by signal, error code = %d\n", rc);
    } else {
      tcase.correct = false;
      tcase.err_desc = prism::FormatString("program exited by other reason");
    }
  }
  
  void OutputStatistics() {
    printf("%s\n", description_.c_str());
    int points = 0;
    for (auto& tcase : testcases_) {
      if (tcase.points == 0) continue;

      if (tcase.correct) tcase.err_desc = "Correct!";

      std::string result;
      prism::StringHelper::Trim(tcase.command, "\r\n", result);
      if (result == "\n") result = "newline";

      double coff = tcase.correct ? 1 : 0;
      if (num_commands_ == 1 && !tcase.correct) {
        /// special judge for testcase 1, which test the support of exit command
        /// in these cases, testcase 1 get 5 points out of 10
        if (!strncmp(tcase.err_desc.c_str(), "pid", 3) ||
            !strncmp(tcase.err_desc.c_str(), "no exit", 7)) {
          coff = 0.5;
        } else if (tcase.err_desc.length() > 0 && pid2_ == pid_) {
          coff = 0.5;
        }
      }

      printf("  %d/%d  command: \"%s\", result: %s\n", static_cast<int>(coff * tcase.points),
             tcase.points,
             result.c_str(),
             tcase.err_desc.c_str());

      points += static_cast<int>(coff * tcase.points);
    }
    printf("Total score = %d/%d\n", points, total_points_);
  }

 private:
  std::string input_file_;

  std::string description_;
  int num_commands_;
  std::vector<TestCase> testcases_;

  int cur_cmd_;
  int total_points_;
  int pid_;
  int pid2_;
  bool restart_;


  JsonHelper json;
};

void sigpipe_handler(int sig) {
  if (sig == SIGPIPE) {
#ifdef JUDGE_DEBUG
    printf("broken pipe\n");
#endif
  }
}

int main(int argc, char* argv[]) {
  pid_t cpid;

  char *value = getenv("JUDGE_BYHAND");
  if (value != nullptr && atoi(value)) {
    g_byhand = true;
  }

  if ((g_byhand && argc != 2) || (!g_byhand && argc != 3)) {
    Usage(argv[0]);
    exit(1);
  }

  signal(SIGPIPE, sigpipe_handler);

  const char* input_file = argc > 2 ? argv[2] : "";
  Grader grader(input_file);
  if (!g_byhand) {
    grader.LoadTest();
  }

restart:
  if (!g_byhand) {
    grader.Restart();
  }

  pp.sp.Create(O_DIRECT);
  pp.rp.Create(O_DIRECT);


  PCHECK(-1 != (cpid = fork()));

  if (cpid == 0) { /// child writes argv[1] to pipe

    close(pp.sp.read_end);
    close(pp.rp.write_end);

    PCHECK(dup2(pp.sp.write_end, 1) >= 0);
    PCHECK(dup2(pp.rp.read_end, 0) >= 0);

    close(pp.sp.write_end);
    close(pp.rp.read_end);
    
    PCHECK(-1 != execlp("unbuffer", "unbuffer", "-p", argv[1], NULL));
    //PCHECK(-1 != execlp(argv[1], argv[1], NULL));
    //PCHECK(-1 != execlp("stdbuf", "stdbuf", "-i0", "-oL", "-eL", argv[1], NULL));

  } else { /// child read from pipe
    /// close unused pipes
    close(pp.sp.write_end);
    close(pp.rp.read_end);

    PCHECK(!SetNonBlocking(pp.sp.read_end, true));

    int rc = 0;
    char line[kLineSize];

    while (!grader.Done()) {
      if (g_byhand) {
        CheckOutput(WriteBuffer);
        fgets(line, kLineSize, stdin);
        if (strcmp(line, "exit\n") == 0) {
          break;
        }
      } else {
        CheckOutput([&grader](char* buffer, size_t size) {
          grader.Check(buffer, size);
        });
        grader.Next(line);
        if (grader.Done()) {
          break;
        }
      }

      rc = write(pp.rp.write_end, line, strlen(line));
      if (rc < 0) {
        // printf("write failed\n");
        break;
      }
    }

    // fprintf(stderr, "%s\n", line);
    if (rc >= 0) {
      rc = write(pp.rp.write_end, line, strlen(line));
      if (g_byhand) {
        CheckOutput(WriteBuffer);
      } else {
        CheckOutput([&grader](char* buffer, size_t size) {
          grader.CheckExitMessage(buffer, size);
        });
      }
    }

    close(pp.sp.read_end);
    close(pp.rp.write_end);

    int status;
    waitpid(cpid, &status, WUNTRACED | WCONTINUED);
    //printf("status = %d\n", status);

    if (!g_byhand) {
      grader.SetExitCode(status);
      if (!grader.Done() && grader.ShouldRestart()) goto restart;

      grader.OutputStatistics();
    }
    exit(0);
  }
}
