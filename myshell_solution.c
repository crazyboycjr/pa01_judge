#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

// Assume that each command line has at most 256 characters (including NULL)
#define MAX_CMDLINE_LEN 256

// Assume that we have at most 16 pipe segments
#define MAX_PIPE_SEGMENTS 16

// Assume that each segment has at most 256 characters (including NULL)
#define MAX_SEGMENT_LENGTH 256

/*
  Function  Prototypes
 */
void show_prompt();
int get_cmd_line(char *cmdline);
void process_cmd(char *cmdline);
void **tokenize(char **argv, char *line, int *numTokens, char *token);


/* The main function implementation */
int main()
{
    char cmdline[MAX_CMDLINE_LEN];
    printf("The shell program (pid=%d) starts\n", getpid());
    while (1)
    {
        show_prompt();
        if (get_cmd_line(cmdline) == -1)
            continue; /* empty line handling */

        process_cmd(cmdline);
    }
    return 0;
}

/* 
    Implementation of process_cmd

    TODO: Clearly explain how you implement process_cmd in point form. For example:

    Step 1: ....
    Step 2: ....
        Step 2.1: .....
        Step 2.2: .....
            Step 2.2.1: .....
    Step 3: ....

 */
void process_cmd(char *cmdline)
{
    char *pipe_segments[MAX_PIPE_SEGMENTS];
    int num_pipe_segments;
    int i,j ;
    int pfds[2];
    pid_t pid;

    // Note: 
    // 0: stdin
    // 1: stdout
    // 2: stderr
    // By default, the previous pipe output is stdin (0)
    int fd_in = 0;  

    tokenize(pipe_segments, cmdline, &num_pipe_segments, "|");

    for (i=0; i<num_pipe_segments; i++) {

        char *pipe_arguments[MAX_SEGMENT_LENGTH];
        int numTokens;
        tokenize(pipe_arguments, pipe_segments[i], &numTokens, " \t\r\n\v\f");


        // Feature 1: Implement the exit command 
        // Hint: Use strcmp to compare character array (char*)
        if ( strcmp(pipe_arguments[0], "exit") == 0 ) {
            printf("The shell program (pid=%d) terminates\n", getpid());
            exit(0);
        }

        // Feature 2: Implement the multi-level pipe
        // Hint:
        //
        //    Think carefully before typing the program
        //    This feature can be implemented by 10-20 lines of source code
        // 
        // Pseudo-code:
        //   (1) Create a message pipe => pfds[0] and pfds[1] will be initialized. 
        //   (2) Call fork()
        //   (3) If it is the child process (i.e. pid == 0)
        //     (3.1) replace stdin (0) using the previous pipe output (i.e. fd_in)
        //     (3.2) If it is not the last pipe segment, replace stdout(1) using the pipe input
        //     (3.3) Close unused pipe output
        //     (3.3) Invoke execvp with appropriate input parameters  
        //   (4) Otherwise, if it is the parent prcoess (i.e. pid > 0)
        //     (4.1) Wait for the child process
        //     (4.2) Close unused pipe input
        //     (4.3) Remember the pipe output (as the next pipe input)
        

        // Note: both pfds[0] and pfds[1] is >2
        pipe(pfds);
        pid = fork();
        if ( pid == 0 ) {  // the child process 

            close(0); // actually, close is optional, but make the code more readable
            dup2(fd_in, 0);   // replace stdin (0) using the previous pipe output
            if (i < num_pipe_segments - 1) { 
                close(1);
                dup2(pfds[1], 1); // replace stdout(1) using pfds[1]
            } 
            close(pfds[0]); // don't need pfds[0]

            execvp(pipe_arguments[0], pipe_arguments);

        } else if (pid > 0) {  // the parent process
            
            wait(0);         // wait for the child process
            close(pfds[1]);  //  don't need pfds[1]
            fd_in = pfds[0];  // remember the pipe output (as the next pipe input)
        }

       
        
    }
       

}

void show_prompt()
{
    printf("$> ");
}

int get_cmd_line(char *cmdline)
{
    int i;
    int n;
    if (!fgets(cmdline, MAX_CMDLINE_LEN, stdin))
        return -1;
    // Ignore the newline character
    n = strlen(cmdline);
    cmdline[--n] = '\0';
    i = 0;
    while (i < n && cmdline[i] == ' ') {
        ++i;
    }
    if (i == n) {
        // Empty command
        return -1;
    }
    return 0;
}

void **tokenize(char **argv, char *line, int *numTokens, char *delimiter)
{
    int argc = 0;
    char *token = strtok(line, delimiter);
    while (token != NULL)
    {
        argv[argc++] = token;
        token = strtok(NULL, delimiter);
    }
    argv[argc++] = NULL;
    *numTokens = argc - 1;
}