
#ifndef PROPERTY_TREE_H_
#define PROPERTY_TREE_H_

#include "./logging.h"

#include <boost/property_tree/ptree.hpp>

namespace pt = boost::property_tree;
using Error = std::string;

class PropertyTree {
 public:
  PropertyTree() {}
  //PropertyTree(pt::ptree root) : root_{root} {}
  virtual ~PropertyTree() noexcept {}

  virtual void Parse(const std::string&) = 0;

  virtual std::string Build() const = 0;

  // pt::ptree& root() { return root_; }

 //public:
  //pt::ptree root_;
};

#endif  // PROPERTY_TREE_H_
