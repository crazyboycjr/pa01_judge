/*!
 *  Copyright (c) 2018 by Contributors
 * \file utils.h
 */
#ifndef PRISM_UTILS_H_
#define PRISM_UTILS_H_
#include <stdlib.h>
#include <cassert>
#include <cstdio>
#include <string>
#include <regex>
#include <vector>

namespace prism {

template <typename... Args>
inline std::string FormatString(const char* fmt, Args... args) {
  int length = std::snprintf(nullptr, 0, fmt, args...);
  assert(length >= 0);

  char* buf = new char[length + 1];
  std::snprintf(buf, length + 1, fmt, args...);

  std::string str(buf);
  delete[] buf;
  return str;
}

class StringHelper {
 public:
  static std::string Dirname(const std::string& name, const char delim = '/') {
    auto pos = name.find_last_of(delim);
    assert(pos != std::string::npos);
    return name.substr(0, pos);
  }

  static std::string Basename(const std::string& name, const char delim = '/') {
    auto pos = name.find_last_of(delim);
    if (pos == std::string::npos) return name;
    return name.substr(pos + 1);
  }

  static std::string Replace(const std::string& input,
                             const std::string& pattern,
                             const std::string& to) {
    return std::regex_replace(input, std::regex(pattern), to);
  }

  static std::string& Trim(const std::string& input, const char* trimset,
                           std::string& result) {
    if (input.length() == 0) {
      result = input;
      return result;
    }

    auto b = input.find_first_not_of(trimset);
    auto e = input.find_last_not_of(trimset);

    if (std::string::npos == b) {
      result = input;
      return result;
    }

    result = std::string(input, b, e - b + 1);
    return result;
  }

  using StringVector = std::vector<std::string>;
  static StringVector& Split(const std::string& input, const char del,
                             StringVector& result, bool remove_empty) {
    std::stringstream ss(input);

    std::string item;
    while (std::getline(ss, item, del)) {
      if (!item.empty() || !remove_empty) {
        result.push_back(item);
      }
    }

    return result;
  }
};

template <typename T>
inline T GetEnvOrDefault(const char* name, T default_value);

#define REGISTER_GET_ENV_FUNC(type, func)                             \
  template <>                                                         \
  inline type GetEnvOrDefault(const char* name, type default_value) { \
    const char* value = getenv(name);                                 \
    if (!value) {                                                     \
      return default_value;                                           \
    }                                                                 \
    return func(value);                                               \
  }

REGISTER_GET_ENV_FUNC(int, atoi)
REGISTER_GET_ENV_FUNC(long, atol)
REGISTER_GET_ENV_FUNC(long long, atoll)
REGISTER_GET_ENV_FUNC(double, atof)
REGISTER_GET_ENV_FUNC(std::string, std::string)
REGISTER_GET_ENV_FUNC(const char*, )

#undef REGISTER_GET_ENV_FUNC

#ifndef DISALLOW_COPY_AND_ASSIGN
#define DISALLOW_COPY_AND_ASSIGN(T) \
  T(T const&) = delete;             \
  T(T&&) = delete;                  \
  T& operator=(T const&) = delete;  \
  T& operator=(T&&) = delete
#endif

}  // namespace prism

#endif  // PRISM_UTILS_H_
