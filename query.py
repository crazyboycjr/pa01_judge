#!/usr/bin/env python3

import sys
import os

DIR = '../pa01_submission'

def main():
    while True:
        try:
            line = input('please input name: ')
        except EOFError:
            sys.exit(0)
        itsc_name = line.strip()
        if len(itsc_name) == 0: continue
        score_file = os.path.join(DIR, itsc_name, 'score.txt')
        with open(score_file, 'r') as f:
            total_points = 0
            for l in f:
                l = l.strip()
                print(l)
                if 'Total' in l:
                    points = l.split(' ')[-1].split('/')[0]
                    total_points += int(points)
            print("Grade:", total_points)

if __name__ == '__main__':
    main()
